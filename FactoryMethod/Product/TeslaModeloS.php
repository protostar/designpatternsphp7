<?php

namespace FactoryMethod\Product;

class TeslaModeloS implements CarroProduct
{
    public function acelerar()
    {
        echo "Acelerar Tesla Modelo S\n";
    }

    public function travar()
    {
        echo "Travar Tesla Modelo S\n";
    }

    public function mudarMudanca()
    {
        echo "Mudar a mudança do Tesla Modelo S\n";
    }
}