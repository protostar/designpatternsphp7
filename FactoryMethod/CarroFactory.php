<?php

namespace FactoryMethod;

use FactoryMethod\Product\CarroProduct;//Desta forma garantimos a compatibilidade de nossas fábricas

interface CarroFactory
{
    public function criarCarro(string $modeloCarro): CarroProduct;
}