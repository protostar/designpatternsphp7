<?php

namespace FactoryMethod\Product;

interface CarroProduct
{
    public function acelerar();

    public function travar();

    public function mudarMudanca();
}