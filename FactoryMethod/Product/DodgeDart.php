<?php

namespace FactoryMethod\Product;

class DodgeDart implements CarroProduct
{
    public function acelerar()
    {
        echo "Acelerar Dodge Dart\n";
    }

    public function travar()
    {
        echo "Travar Dodge Dart\n";
    }

    public function mudarMudanca()
    {
        echo "Mudar a mudança do Dodge Dart\n";
    }
}