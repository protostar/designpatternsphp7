<?php

namespace FactoryMethod\Product;

class DodgeCharger implements CarroProduct
{
    public function acelerar()
    {
        echo "Acelerar Dodge Charger\n";
    }

    public function travar()
    {
        echo "Travar Dodge Charger\n";
    }

    public function mudarMudanca()
    {
        echo "Mudar a mudança do Dodge Charger\n";
    }
}