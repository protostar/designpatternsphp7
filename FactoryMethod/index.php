<?php

require_once('../autoloader.php');

$teslaFactory = new \FactoryMethod\TeslaFactory();

$dodgeFactory = new \FactoryMethod\DodgeFactory();

try {
    $teslaModeloS = $teslaFactory->criarCarro('modelo_s');
    $teslaModeloX = $teslaFactory->criarCarro('modelo_x');

    $dodgeCharger = $dodgeFactory->criarCarro('charger');
    $dodgeDart = $dodgeFactory->criarCarro('dart');
} catch (Exception $e) {
    echo $e->getMessage();
}

echo $teslaModeloS->acelerar();
echo $teslaModeloS->travar();
echo $teslaModeloS->mudarMudanca();
echo "\n";

echo $dodgeCharger->acelerar();
echo $dodgeCharger->travar();
echo $dodgeCharger->mudarMudanca();
echo "\n";

echo $teslaModeloX->acelerar();
echo $teslaModeloX->travar();
echo $teslaModeloX->mudarMudanca();
echo "\n";

echo $dodgeDart->acelerar();
echo $dodgeDart->travar();
echo $dodgeDart->mudarMudanca();
