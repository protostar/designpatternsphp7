<?php

namespace FactoryMethod\Product;

class TeslaModeloX implements CarroProduct
{
    public function acelerar()
    {
        echo "Acelerar Tesla Modelo X\n";
    }

    public function travar()
    {
        echo "Travar Tesla Modelo X\n";
    }

    public function mudarMudanca()
    {
        echo "Mudar a mudança do Tesla Modelo X\n";
    }
}